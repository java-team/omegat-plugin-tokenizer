OmegaT-tokenizers.jar is distributed under the GPLv3 license
lucene-*.jar are distributed under the Apache License 2.0
The English tokenizer (org.omegat.plugins.tokenizer.SnowballEnglishTokenizer) uses
stop words from Okapi (http://okapi.sourceforge.net) under the LGPL license.