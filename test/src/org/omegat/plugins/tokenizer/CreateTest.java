/**************************************************************************
 Custom tokenizers components for OmegaT(http://www.omegat.org/)
 
 Copyright (C) 2008 Alex Buloichik (alex73mail@gmail.com)

 Code is released under the dual licenses of the GPLv3 and Apache License v2.0
 *************************************************************************
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *************************************************************************
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 *************************************************************************/
package org.omegat.plugins.tokenizer;

import java.io.File;
import java.io.FileFilter;

import junit.framework.TestCase;

public class CreateTest extends TestCase {
    public void testCreateAllTokenizers() throws Exception {
        File[] files = new File("src/org/omegat/plugins/tokenizer/")
                .listFiles(new FileFilter() {
                    public boolean accept(File pathname) {
                        String name = pathname.getName();
                        return name.endsWith(".java")
                                && (name.startsWith("Lucene") || name
                                        .startsWith("Snowball"));
                    }
                });
        for (File f : files) {
            BaseTokenizer tok = (BaseTokenizer) Class.forName(
                    "org.omegat.plugins.tokenizer."
                            + f.getName().replaceAll("\\.java", ""))
                    .newInstance();
            assertNotNull(tok.getTokenStream("test", true, false));
            assertNotNull(tok.getTokenStream("test", false, false));
        }
    }
}
