/**************************************************************************
 Custom tokenizers components for OmegaT(http://www.omegat.org/)
 
 Copyright (C) 2008 Alex Buloichik (alex73mail@gmail.com)

 Code is released under the dual licenses of the GPLv3 and Apache License v2.0
 *************************************************************************
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *************************************************************************
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 *************************************************************************/
package org.omegat.plugins.tokenizer;

import java.io.IOException;
import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.lucene.analysis.TokenStream;
import org.omegat.core.CoreEvents;
import org.omegat.core.events.IProjectEventListener;
import org.omegat.core.matching.ITokenizer;
import org.omegat.core.matching.WordIterator;
import org.omegat.util.Token;

/**
 * Base class for Lucene-based tokenizers.
 * 
 * @author Alex Buloichik (alex73mail@gmail.com)
 */
public abstract class BaseTokenizer implements ITokenizer {
    private static Map<String, Token[]> tokenCacheNone = new HashMap<String, Token[]>(
            5000);
    private static Map<String, Token[]> tokenCacheMatching = new HashMap<String, Token[]>(
            5000);
    private static Map<String, Token[]> tokenCacheGlossary = new HashMap<String, Token[]>(
            5000);

    protected static final String[] EMPTY_STOP_WORDS_LIST = new String[0];
    protected static final Token[] EMPTY_TOKENS_LIST = new Token[0];
    protected static final int DEFAULT_TOKENS_COUNT = 64;

    public BaseTokenizer() {
        CoreEvents.registerProjectChangeListener(new IProjectEventListener() {
            public void onProjectChanged(PROJECT_CHANGE_TYPE eventType) {
                if (eventType == PROJECT_CHANGE_TYPE.CLOSE) {
                    synchronized (tokenCacheNone) {
                        tokenCacheNone.clear();
                    }
                    synchronized (tokenCacheMatching) {
                        tokenCacheMatching.clear();
                    }
                    synchronized (tokenCacheGlossary) {
                        tokenCacheGlossary.clear();
                    }
                }
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    public Token[] tokenizeWordsForSpelling(String str) {
        return tokenize(str, false, false);
    }

    /**
     * {@inheritDoc}
     */
    public Token[] tokenizeWords(final String strOrig,
            final StemmingMode stemmingMode) {
        Map<String, Token[]> cache = null;
        switch (stemmingMode) {
        case NONE:
            cache = tokenCacheNone;
            break;
        case GLOSSARY:
            cache = tokenCacheGlossary;
            break;
        case MATCHING:
            cache = tokenCacheMatching;
            break;
        }
        Token[] result;
        synchronized (cache) {
            result = cache.get(strOrig);
        }
        if (result != null)
            return result;

        result = tokenize(strOrig, stemmingMode == StemmingMode.GLOSSARY
                || stemmingMode == StemmingMode.MATCHING,
                stemmingMode == StemmingMode.MATCHING);

        // put result in the cache
        synchronized (cache) {
            cache.put(strOrig, result);
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    public Token[] tokenizeAllExactly(final String strOrig) {
        if (strOrig.length() == 0) {
            return EMPTY_TOKENS_LIST;
        }

        List<Token> result = new ArrayList<Token>(DEFAULT_TOKENS_COUNT);

        WordIterator iterator = new WordIterator();
        iterator.setText(strOrig.toLowerCase());

        int start = iterator.first();
        for (int end = iterator.next(); end != BreakIterator.DONE; start = end, end = iterator
                .next()) {
            String tokenStr = strOrig.substring(start, end).toLowerCase();
            result.add(new Token(tokenStr, start));
        }

        return result.toArray(new Token[result.size()]);
    }

    protected Token[] tokenize(final String strOrig,
            final boolean stemsAllowed, final boolean stopWordsAllowed) {
        if (strOrig == null || strOrig.length() == 0) {
            return EMPTY_TOKENS_LIST;
        }

        List<Token> result = new ArrayList<Token>(64);

        final TokenStream in = getTokenStream(strOrig, stemsAllowed,
                stopWordsAllowed);

        org.apache.lucene.analysis.Token tok;
        try {
            while ((tok = in.next()) != null) {
                String tokenText = tok.termText();
                for (int i = 0; i < tokenText.length(); i++) {
                    if (Character.isDigit(tokenText.charAt(i))) {
                        tokenText = null;
                        break;
                    }
                }
                if (tokenText != null) {
                    result.add(new Token(tokenText, tok.startOffset(), tok
                            .endOffset()
                            - tok.startOffset()));
                }
            }
        } catch (IOException ex) {
            // shouldn't happen
        }
        return result.toArray(new Token[result.size()]);
    }

    protected abstract TokenStream getTokenStream(final String strOrig,
            final boolean stemsAllowed, final boolean stopWordsAllowed);
}
