/**************************************************************************
 Custom tokenizers components for OmegaT(http://www.omegat.org/)
 
 Copyright (C) 2008 Alex Buloichik (alex73mail@gmail.com)

 Code is released under the dual licenses of the GPLv3 and Apache License v2.0
 *************************************************************************
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *************************************************************************
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 *************************************************************************/
package org.omegat.plugins.tokenizer;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.snowball.SnowballAnalyzer;
import org.apache.lucene.analysis.standard.StandardTokenizer;

/**
 * @author Alex Buloichik (alex73mail@gmail.com)
 */
public class SnowballEnglishTokenizer extends BaseTokenizer {
    public static final String[] STOP_WORDS;

    static {
        // Load stopwords
        try {
            InputStream in = SnowballEnglishTokenizer.class
                    .getResourceAsStream("StopList_en.txt");
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(
                        in, "UTF-8"));
                String s;
                List<String> words = new ArrayList<String>();
                while ((s = rd.readLine()) != null) {
                    s = s.trim();
                    if (s.length() == 0 || s.startsWith("#")) {
                        continue;
                    }
                    words.add(s);
                }
                STOP_WORDS = words.toArray(new String[words.size()]);
            } finally {
                in.close();
            }
        } catch (Exception ex) {
            throw new ExceptionInInitializerError(
                    "Error load stopwords in SnowballEnglishTokenizer: "
                            + ex.getMessage());
        }
    }

    @Override
    protected TokenStream getTokenStream(final String strOrig,
            final boolean stemsAllowed, final boolean stopWordsAllowed) {
        if (stemsAllowed) {
            return new SnowballAnalyzer("English", STOP_WORDS).tokenStream(
                    null, new StringReader(strOrig));
        } else {
            return new StandardTokenizer(
                    new StringReader(strOrig.toLowerCase()));
        }
    }
}
