OmegaT-tokenizers provides better fuzzy and glossary matches to OmegaT, by 
computing the roots (stemming) of source words. 

Installation:
Unzip the files, and put them all in a "plugins" folder located in OmegaT's 
installation folder (where OmegaT.jar is installed). If the "plugins" folder 
does not exist, create it first.

You can then edit the launching script (or create one, e.g., OmegaT.bat
for Windows, or OmegaT or OmegaT.sh for Linux or Mac) to select the required 
tokenizer.

To do this, add "--ITokenizer=..." into the command line, where "..." is one 
of the following tokenizers:

org.omegat.plugins.tokenizer.LuceneArabicTokenizer
org.omegat.plugins.tokenizer.LuceneBrazilianTokenizer
org.omegat.plugins.tokenizer.LuceneChineseTokenizer
org.omegat.plugins.tokenizer.LuceneCJKTokenizer
org.omegat.plugins.tokenizer.LuceneCzechTokenizer
org.omegat.plugins.tokenizer.LuceneDutchTokenizer
org.omegat.plugins.tokenizer.LuceneFrenchTokenizer
org.omegat.plugins.tokenizer.LuceneGermanTokenizer
org.omegat.plugins.tokenizer.LuceneGreekTokenizer
org.omegat.plugins.tokenizer.LucenePersianTokenizer
org.omegat.plugins.tokenizer.LuceneSmartChineseTokenizer
org.omegat.plugins.tokenizer.LuceneRussianTokenizer
org.omegat.plugins.tokenizer.LuceneThaiTokenizer
org.omegat.plugins.tokenizer.SnowballDanishTokenizer
org.omegat.plugins.tokenizer.SnowballDutchTokenizer
org.omegat.plugins.tokenizer.SnowballEnglishTokenizer
org.omegat.plugins.tokenizer.SnowballFinnishTokenizer
org.omegat.plugins.tokenizer.SnowballFrenchTokenizer
org.omegat.plugins.tokenizer.SnowballGerman2Tokenizer
org.omegat.plugins.tokenizer.SnowballGermanTokenizer
org.omegat.plugins.tokenizer.SnowballHungarianTokenizer
org.omegat.plugins.tokenizer.SnowballItalianTokenizer
org.omegat.plugins.tokenizer.SnowballNorwegianTokenizer
org.omegat.plugins.tokenizer.SnowballPorterTokenizer
org.omegat.plugins.tokenizer.SnowballPortugueseTokenizer
org.omegat.plugins.tokenizer.SnowballRomanianTokenizer
org.omegat.plugins.tokenizer.SnowballRussianTokenizer
org.omegat.plugins.tokenizer.SnowballSpanishTokenizer
org.omegat.plugins.tokenizer.SnowballSwedishTokenizer
org.omegat.plugins.tokenizer.SnowballTurkishTokenizer

Examples

German source:
java -jar OmegaT.jar --ITokenizer=org.omegat.plugins.tokenizer.LuceneGermanTokenizer

English source:
java -jar OmegaT.jar --ITokenizer=org.omegat.plugins.tokenizer.SnowballEnglishTokenizer

If you wish to use different tokenizers because you translate from more 
than one language, create a separate OmegaT launch script file for each 
tokenizer you wish to use. Name the launch script files appropriately, 
for example "OmegaT-DE.bat" for the launch script file containing the 
command with the German tokenizer and "OmegaT-EN.bat" for the launch 
script file containing the command with the English tokenizer.

